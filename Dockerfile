FROM willat8/hs-buildenv-debian-rpi3:20170720

RUN apt-get install -y \
    git \
    autoconf \
    pkg-config \
    libtool \
    make

# Install the weston build dependencies
RUN apt-get install -y \
    libweston-1-dev \
    libgles2-mesa-dev \
    libcairo2-dev \
    libxcb-composite0-dev \
    libxcursor-dev \
    libmtdev-dev \
    libgbm-dev \
    libudev-dev \
    libinput-dev \
    libwayland-dev \
    libpam-dev

RUN git -C /usr/src clone https://github.com/wayland-project/wayland-protocols.git

WORKDIR /usr/src/wayland-protocols

RUN ./autogen.sh --prefix=/usr \
 && make -j9 \
 && make install

RUN git -C /usr/src clone https://github.com/wayland-project/weston.git

WORKDIR /usr/src/weston

RUN ./autogen.sh \
 && make -j9

RUN apt-get install -y libghc-cairo-dev libghc-http-conduit-dev cabal-install

RUN cabal update && \
    cabal install --global bitwise

